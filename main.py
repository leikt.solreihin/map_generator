#! /usr/bin/env python3
# coding: utf-8

import map_generator

import tests
import src
import src.heightmap_generation

def main():
    map_generator.run()

if __name__ == "__main__":
    main()